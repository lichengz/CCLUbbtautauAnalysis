#!/usr/bin/env bash

#############################################
#  DO NOT RUN MANUALLY!                     #
#  This is invoked by the setup.sh script!  #
#############################################

# Define function to check git repositories
check_git_repos() {

    # Check CCLUbbtautauAnalysis
    cd $CMT_BASE/../
    git fetch origin --quiet
    CCLUB_STATUS=$(git status)
    cd -

    # Check NBA main package
    git fetch origin --quiet
    NBA_STATUS=$(git status)

    # Check NBA sub-packages
    cd "$CMT_CMSSW_BASE/$CMT_CMSSW_VERSION/src"

    cd PhysicsTools/NanoAODTools
    git fetch origin --quiet
    NANOTOOLS_STATUS=$(git status)
    cd ../..
    cd Base/Modules
    git fetch origin --quiet
    BASEMODULES_STATUS=$(git status)
    cd ../..
    cd Base/Filters
    git fetch origin --quiet
    BASEFILTERS_STATUS=$(git status)
    cd ../..
    cd HHKinFit2
    git fetch origin --quiet
    HHKINFIT_STATUS=$(git status)
    cd ..
    cd TauAnalysis/ClassicSVfit
    git fetch origin --quiet
    CLASSSVFIT_STATUS=$(git status)
    cd ../..
    cd TauAnalysis/SVfitTF
    git fetch origin --quiet
    SVFITTF_STATUS=$(git status)
    cd ../..
    cd HTT-utilities
    git fetch origin --quiet
    HTTUTILS_STATUS=$(git status)
    cd LepEffInterface/data
    git fetch origin --quiet
    LEPTEFF_STATUS=$(git status)
    cd ../../..
    cd TauAnalysisTools/TauTriggerSFs
    git fetch origin --quiet
    TRIGSF_STATUS=$(git status)
    cd ../..
    cd HHTools/HHbtag
    git fetch origin --quiet
    HHBTAG_STATUS=$(git status)
    cd ../..
    cd Tools/Tools
    git fetch origin --quiet
    ANACORE_STATUS="$(git status)"
    cd ../..
    cd MulticlassInference
    git fetch origin --quiet
    MULTICL_STATUS="$(git status)"
    cd ..
    cd cms_hh_proc_interface
    git fetch origin --quiet
    HHPROCINTERF_STATUS="$(git status)"
    cd ..
    cd cms_hh_tf_inference
    git fetch origin --quiet
    HHTFINTERF_STATUS="$(git status)"
    cd ..
    cd cms_runII_dnn_models
    git fetch origin --quiet
    RUN2MODELS_STATUS="$(git status)"
    cd ..

    # Check Corrections
    cd Corrections
    cd TAU
    git fetch origin --quiet
    TAU_STATUS="$(git status)"
    cd ..
    cd JME
    git fetch origin --quiet
    JME_STATUS="$(git status)"
    cd ..
    cd LUM
    git fetch origin --quiet
    LUM_STATUS="$(git status)"
    cd ..
    cd MUO
    git fetch origin --quiet
    MUO_STATUS="$(git status)"
    cd ..
    cd EGM
    git fetch origin --quiet
    EGM_STATUS="$(git status)"
    cd ..
    cd BTV
    git fetch origin --quiet
    BTV_STATUS="$(git status)"
    cd ../..

    # Check Combine
    cd HiggsAnalysis/CombinedLimit
    git fetch origin --quiet
    COMBINE_STATUS="$(git status)"
    cd ../..
    cd CombineHarvester
    git fetch origin --quiet
    COMBHARV_STATUS="$(git status)"
    cd ..

    # Printout colors
    RED='\033[0;31m' # Red
    GRE='\033[0;32m' # Green
    YEL='\033[0;33m' # Yellow
    CYA='\033[0;36m' # Cyan
    NCO='\033[0m'    # No Color

    # Check if repo is "up to date" or "behind" origin
    function check_status {
        if [[ $1 =~ "behind" ]]; then
            if [[ $2 =~ "HHbtag" ]]; then
                echo -e " - $2: ${YEL}HHbtag is temporarily rolled back to a previous version!${NCO}"
            else
                echo -e " - $2: ${RED}Outdated!\n To fix this, please do:${NCO}"
                if [[ $2 =~ "NBA" ]]; then
                    echo -e "     ${CYA} cd nanoaod_base_analysis"
                    echo -e "     ${CYA} git pull origin"
                    echo -e "     ${CYA} cd -"
                    echo -e "     ${CYA} source setup.sh${NCO}"
                elif [[ $2 =~ "CCLUb" ]]; then
                    echo -e "     ${CYA} git pull origin"
                    echo -e "     ${CYA} source setup.sh${NCO}"
                else
                    echo -e "     ${CYA} cd "\$CMT_CMSSW_BASE/\$CMT_CMSSW_VERSION/src/$2" "
                    echo -e "     ${CYA} git pull origin"
                    echo -e "     ${CYA} cd -"
                    echo -e "     ${CYA} source setup.sh${NCO}"
                fi
            fi
        elif [[ $1 =~ "up to date" ]]; then
            echo -e " - $2: ${GRE}OK${NCO}"
        elif [[ $1 =~ "diverged" ]]; then
            echo -e " - $2: ${RED}Diverged!\n Please fix this as soon as possible! Do:"
            echo -e "     ${CYA} cd "\$CMT_CMSSW_BASE/\$CMT_CMSSW_VERSION/src/$2" "
            echo -e "     ${CYA} git status"
            echo -e " ${RED}and follow the suggestions.${NCO}"
        elif [[ $1 =~ "ahead" ]]; then
            echo -e " - $2: ${GRE}AHEAD ${CYA}(Please, consider opening a PR)${NCO}"
        elif [[ $2 =~ "Combine" ]]; then
            echo -e " - $2: ${YEL}Excluded from checks!${NCO}"
        elif [[ $2 =~ "hh_proc" ]]; then
            echo -e " - $2: ${YEL}Excluded from checks!${NCO}"
        fi
    }

    # Run checks for all repos
    echo "------ Checking CCLUB sub-dependencies ------"
    check_status "$CCLUB_STATUS"        "CCLUbbtautauAnalysis              "
    check_status "$NBA_STATUS"          "NBA                               "
    check_status "$NANOTOOLS_STATUS"    "PhysicsTools/NanoAODTools         "
    check_status "$BASEMODULES_STATUS"  "Base/Modules                      "
    check_status "$BASEFILTERS_STATUS"  "Base/Filters                      "
    check_status "$HHKINFIT_STATUS"     "HHKinFit2                         "
    check_status "$CLASSSVFIT_STATUS"   "TauAnalysis/ClassicSVfit          "
    check_status "$SVFITTF_STATUS"      "TauAnalysis/SVfitTF               "
    check_status "$HTTUTILS_STATUS"     "HTT-utilities                     "
    check_status "$LEPTEFF_STATUS"      "HTT-utilities/LepEffInterface/data"
    check_status "$TRIGSF_STATUS"       "TauAnalysisTools/TauTriggerSFs    "
    check_status "$HHBTAG_STATUS"       "HHTools/HHbtag                    "
    check_status "$ANACORE_STATUS"      "Tools/Tools                       "
    check_status "$MULTICL_STATUS"      "MulticlassInference               "
    check_status "$HHPROCINTERF_STATUS" "cms_hh_proc_interface             "
    check_status "$HHTFINTERF_STATUS"   "cms_hh_tf_inference               "
    check_status "$RUN2MODELS_STATUS"   "cms_runII_dnn_models              "
    check_status "$TAU_STATUS"          "Corrections/TAU                   "
    check_status "$JME_STATUS"          "Corrections/JME                   "
    check_status "$LUM_STATUS"          "Corrections/LUM                   "
    check_status "$MUO_STATUS"          "Corrections/MUO                   "
    check_status "$EGM_STATUS"          "Corrections/EGM                   "
    check_status "$BTV_STATUS"          "Corrections/BTV                   "
    check_status "$COMBINE_STATUS"      "HiggsAnalysis/CombinedLimit       "
    check_status "$COMBHARV_STATUS"     "CombineHarvester                  "
    echo "---------------------------------------------"

    # Back to nanoaod_base_analysis
    cd ../../../..
}

# Run function
check_git_repos
