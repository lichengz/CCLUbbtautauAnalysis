from analysis_tools import ObjectCollection, Category, Process, Dataset, Feature, Systematic
from analysis_tools.utils import DotDict
from analysis_tools.utils import join_root_selection as jrs
from plotting_tools import Label
from collections import OrderedDict

from config.run3_base_config import Config as base_config
from config.run3_dataset_2022_preEE import Datasets_2022_preEE as dataset_config

class Config_2022_preEE(base_config, dataset_config):
    def __init__(self, *args, **kwargs):
        super(Config_2022_preEE, self).__init__(*args, **kwargs)

    def add_weights(self):
        weights = DotDict()
        weights.default = "1"
        weights.total_events_weights = ["genWeight", "puWeight"]

        weights.corrections = ["genWeight", "puWeight", "trigSF",
                               "idAndIsoAndFakeSF", "bTagweightReshape"]

        weights.base = weights.corrections
        weights.baseline = weights.corrections
        weights.base_selection = weights.corrections

        weights.mutau = weights.corrections
        weights.etau = weights.corrections
        weights.tautau = weights.corrections

        return weights

    def add_default_module_files(self):
        defaults = {}
        defaults["PreprocessRDF"] = "run3_2022_modulesrdf"
        defaults["PreCounter"] = "run3_weights"
        return defaults

config = Config_2022_preEE("Config_2022_preEE", year=2022, runPeriod="preEE", ecm=13.6, lumi_pb=8077)